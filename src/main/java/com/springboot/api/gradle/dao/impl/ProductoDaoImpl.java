package com.springboot.api.gradle.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.gradle.dao.ProductoDao;
import com.springboot.api.gradle.model.Producto;
import com.springboot.api.gradle.rowmapper.ProductoRowMapper;

@Repository
public class ProductoDaoImpl extends JdbcDaoSupport implements ProductoDao {
	public ProductoDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}

	@Override
	public List<Producto> getAllProducto() {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Producto> listaProductos = new ArrayList<Producto>();
		
		String sql = " SELECT id, descripcion, categoria, precio_unitario, stock_actual, stock_minimo, estado\n" + 
				" FROM microservicios.producto";
		
		try {
			
			RowMapper<Producto> productoRow = new ProductoRowMapper();
			listaProductos = getJdbcTemplate().query(sql, productoRow);
			logger.debug("Se han listado "+listaProductos.size()+" productos");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaProductos;
	}

	@Override
	public Producto getProducto(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveProducto(Producto producto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteProducto(Integer id) {
		// TODO Auto-generated method stub
		
	}
	
	
}
