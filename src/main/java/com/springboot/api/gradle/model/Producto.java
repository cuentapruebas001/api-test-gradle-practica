package com.springboot.api.gradle.model;

public class Producto {
	private Integer id;
	private String descripcion;
	private String categoria;
	private Integer precio_unitario;
	private Integer stock_actual;
	private Integer stock_minimo;
	private Integer estado;
	
	public Producto() {
		
	}

	public Producto(Integer id, String descripcion, String categoria, Integer precio_unitario, Integer stock_actual,
			Integer stock_minimo, Integer estado) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.precio_unitario = precio_unitario;
		this.stock_actual = stock_actual;
		this.stock_minimo = stock_minimo;
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Producto [id=" + id + ", descripcion=" + descripcion + ", categoria=" + categoria + ", precio_unitario="
				+ precio_unitario + ", stock_actual=" + stock_actual + ", stock_minimo=" + stock_minimo + ", estado="
				+ estado + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getPrecio_unitario() {
		return precio_unitario;
	}

	public void setPrecio_unitario(Integer precio_unitario) {
		this.precio_unitario = precio_unitario;
	}

	public Integer getStock_actual() {
		return stock_actual;
	}

	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}

	public Integer getStock_minimo() {
		return stock_minimo;
	}

	public void setStock_minimo(Integer stock_minimo) {
		this.stock_minimo = stock_minimo;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	
}
